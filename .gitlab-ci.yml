stages:
- build
- deploy

variables:
  LINUX_64_IMAGE: "agrumery/manylinux_2_28_x86_64"
  LINUX_WHEELHOUSE: "wrappers/pyAgrum/wheelhouse/build_wheels_manylinux.sh"
  OSX_WHEELHOUSE: "wrappers/pyAgrum/wheelhouse/build_wheels_macos.sh"
  ACT_OPTIONS: "--no-fun --compiler=mvsc22 -d build"


################################################################################
# Linux / gcc
################################################################################

linux_agrum:
  image: agrumery/ubuntu_py311
  tags:
    - docker
    - linux
    - triskele
  cache:
    key: linux
    paths:
      - build/
  stage: build
  script:
    - python3 act clean
    - python3 act --no-fun test release aGrUM -j halfexcept1

linux_pyagrum_py38:
  image: agrumery/ubuntu_py38
  tags:
    - docker
    - linux
    - triskele
  cache:
    key: linux
    paths:
      - build/
  stage: build
  script:
    - python3 act clean
    - pip3 install -r wrappers/pyAgrum/testunits/requirements.txt
    - python3 act --no-fun test release pyAgrum -j halfexcept1 -t quick

linux_pyagrum_py311:
  image: agrumery/ubuntu_py311
  tags:
    - docker
    - linux
    - triskele
  cache:
    key: linux
    paths:
      - build/
  stage: build
  script:
    - python3 act clean
    - pip3 install -r wrappers/pyAgrum/testunits/requirements.txt
    - python3 act --no-fun test release pyAgrum -j halfexcept1 -t quick


################################################################################
# Wheels on tags and merge requests
################################################################################


build_wheel_manylinux_py38:
  only:
    - tags
    - master@agrumery/aGrUM
    - merge_requests
  image: $LINUX_64_IMAGE
  tags:
    - docker
    - linux
    - manylinux1
  stage: build
  script:
    - bash $LINUX_WHEELHOUSE cp38-cp38

build_wheel_manylinux_py311:
  only:
    - tags
    - master@agrumery/aGrUM
    - merge_requests
  image: $LINUX_64_IMAGE
  tags:
    - docker
    - linux
    - manylinux1
  stage: build
  script:
    - bash $LINUX_WHEELHOUSE cp311-cp311

build_wheel_osx_py38:
  only:
    - tags
    - master@agrumery/aGrUM
    - merge_requests
  tags:
    - macos
    - python3
  stage: build
  script:
    - bash $OSX_WHEELHOUSE py38

build_wheel_osx_py311:
  only:
    - tags
    - master@agrumery/aGrUM
    - merge_requests
  tags:
    - macos
    - python3
  stage: build
  script:
    - bash $OSX_WHEELHOUSE py311
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_REF_NAME}"
    when: on_success
    expire_in: 1 week
    paths:
      - wheels/*.whl

build_wheel_windows_py38:
  only:
    - tags
    - master@agrumery/aGrUM
    - merge_requests
  tags:
    - windows
    - conda
    - agrumerie2
  stage: build
  variables:
    PATH: $PATH_64_MVSC22_AGRUMERIE2
  script:
    - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.8
    - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
    - call conda.bat install -y -c conda-forge --file "%CI_PROJECT_DIR%\wrappers\pyAgrum\testunits\requirements.txt"
    - call conda.bat install -y -c conda-forge cmake
    - python %CI_PROJECT_DIR%\wrappers\pyAgrum\wheelhouse\scripts\update_version.py %CI_PROJECT_DIR% 0
    - cd "%CI_PROJECT_DIR%\aGrUM" & python act %ACT_OPTIONS% release wheel pyAgrum -j halfexcept1
    - call conda.bat activate "py37"
    - call conda.bat remove -y -n "gitlab-ci-%CI_JOB_ID%" --all --force

build_wheel_windows_py311:
  only:
    - tags
    - master@agrumery/aGrUM
    - merge_requests
  tags:
    - windows
    - conda
    - agrumerie2
  stage: build
  variables:
    PATH: $PATH_64_MVSC22_AGRUMERIE2
  script:
    - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.11
    - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
    - pip install -r "%CI_PROJECT_DIR%\wrappers\pyAgrum\testunits\requirements.txt"
    - call conda.bat install -y -c conda-forge cmake
    - python %CI_PROJECT_DIR%\wrappers\pyAgrum\wheelhouse\scripts\update_version.py %CI_PROJECT_DIR% 0
    - cd "%CI_PROJECT_DIR%\aGrUM" & python act %ACT_OPTIONS% release wheel pyAgrum -j halfexcept1
    - call conda.bat activate "py37"
    - call conda.bat remove -y -n "gitlab-ci-%CI_JOB_ID%" --all --force

################################################################################
# MacOS / clang
################################################################################

macos_agrum:
  only:
    - master@agrumery/aGrUM
    - web@agrumery/aGrUM
    - merge_requests
  tags:
    - macos
    - conda
  variables:
    CC: "/usr/bin/clang"
    CXX: "/usr/bin/clang++"
  stage: build
  cache:
    key: macos
    paths:
      - build/
  script:
    - . /Users/agrum/miniconda3/etc/profile.d/conda.sh
    - conda activate py311
    - python act clean
    - python act --no-fun test release aGrUM -j halfexcept1
    - conda deactivate

macos_pyagrum:
  only:
    - master@agrumery/aGrUM
    - web@agrumery/aGrUM
    - merge_requests
  tags:
    - macos
    - conda
  variables:
    CC: "/usr/bin/clang"
    CXX: "/usr/bin/clang++"
  stage: build
  cache:
    key: macos
    paths:
      - build/
  script:
    - . /Users/agrum/miniconda3/etc/profile.d/conda.sh
    - conda activate py311
    - python act clean
    - python act --no-fun test release pyAgrum -t quick -j halfexcept1
    - conda deactivate

################################################################################
# Windows / MVSC
################################################################################

windows_agrum_2019:
  only:
   - master@agrumery/aGrUM
   - web@agrumery/aGrUM
   - merge_requests
  tags:
    - windows
    - conda
    - agrumerie2
  variables:
    PATH: $PATH_64_MVSC19_AGRUMERIE2
  cache:
    key: mvsc
    paths:
      - build/
  stage: build
  script:
    - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.8
    - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
    - call conda.bat install -y -c conda-forge --file wrappers\pyAgrum\testunits\requirements.txt
    - call pip install cmake
    - python act clean
    - python act --no-fun --compiler=mvsc19 -d build -j except1 test release aGrUM 
    - call conda.bat deactivate
    - call conda.bat remove -n "gitlab-ci-%CI_JOB_ID%" --all --force -y
  retry: 1

windows_agrum_2022:
  only:
   - master@agrumery/aGrUM
   - web@agrumery/aGrUM
   - merge_requests
  tags:
    - windows
    - conda
    - agrumerie2
  variables:
    PATH: $PATH_64_MVSC22_AGRUMERIE2
  cache:
    key: mvsc
    paths:
      - build/
  stage: build
  script:
    - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.8
    - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
    - call conda.bat install -y -c conda-forge --file wrappers\pyAgrum\testunits\requirements.txt
    - call pip install cmake
    - python act clean
    - python act --no-fun --compiler=mvsc22 -d build -j except1 test release aGrUM 
    - call conda.bat deactivate
    - call conda.bat remove -n "gitlab-ci-%CI_JOB_ID%" --all --force -y
  retry: 1

# windows_pyagrum_2019:
#   only:
#    - master@agrumery/aGrUM
#    - web@agrumery/aGrUM
#    - merge_requests
#   tags:
#     - windows
#     - conda
#     - agrumerie2
#   variables:
#     PATH: $PATH_64_MVSC19_AGRUMERIE2
#   cache:
#     key: mvsc19
#     paths:
#       - build/
#   stage: build
#   script:
#     - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.11
#     - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
#     - call pip install -r wrappers\pyAgrum\testunits\requirements.txt
#     - call pip install cmake
#     - python act clean
#     - python act --no-fun test release pyAgrum --no-fun --compiler=mvsc19 -d build -j halfexcept1 -t quick
#     - call conda.bat deactivate
#     - call conda.bat remove -n "gitlab-ci-%CI_JOB_ID%" --all --force -y
#   retry: 2

windows_pyagrum_2022:
  only:
   - master@agrumery/aGrUM
   - web@agrumery/aGrUM
   - merge_requests
  tags:
    - windows
    - conda
    - agrumerie2
  variables:
    PATH: $PATH_64_MVSC22_AGRUMERIE2
  cache:
    key: mvsc
    paths:
      - build/
  stage: build
  script:
    - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.11
    - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
    - call pip install -r wrappers\pyAgrum\testunits\requirements.txt
    - call pip install cmake
    - python act clean
    - python act --no-fun test release pyAgrum --no-fun --compiler=mvsc22 -d build -j except1 -t quick
    - call conda.bat deactivate
    - call conda.bat remove -n "gitlab-ci-%CI_JOB_ID%" --all --force -y
  retry: 1

################################################################################
# Windows / MINGW
################################################################################

windows_pyagrum_mingw:
  only:
   - master@agrumery/aGrUM
   - web@agrumery/aGrUM
   - merge_requests
  tags:
    - windows
    - conda
    - agrumerie2
  variables:
    PATH: $PATH_64_MINGW_AGRUMERIE2
  cache:
    key: mingw
    paths:
      - build/
  stage: build
  script:
    - call conda.bat create -n "gitlab-ci-%CI_JOB_ID%" -c conda-forge python=3.11
    - call conda.bat activate "gitlab-ci-%CI_JOB_ID%"
    - call pip install -r wrappers\pyAgrum\testunits\requirements.txt
    - call pip install cmake
    - python act clean
    - python act install release pyAgrum --compiler=mingw64 -d build -j except1 -t quick
    - '@For %%A In (libgcc_s_seh-1 libgomp-1 libssp-0 libstdc++-6 libwinpthread-1) Do @Copy /-Y "C:\msys64\mingw64\bin\%%A.dll" %CD%"\build\pyAgrum\release\wrappers\pyAgrum"'
    - python act test release pyAgrum
    - call conda.bat deactivate
    - call conda.bat remove -n "gitlab-ci-%CI_JOB_ID%" --all --force -y
  retry: 1

################################################################################
# Deploy  wheels
################################################################################

linux_build:
  image: agrumery/ubuntu
  only:
    - tags@agrumery/aGrUM
  tags:
    - docker
    - linux
  cache:
    key: linux
    paths:
      - build/
  stage: deploy
  script:
    - apt-get install -y curl
    - curl -X POST -F token=${AGRUM_DEPLOY_TOKEN} -F ref=master -F "variables[AGRUM_TAG]=${CI_COMMIT_TAG}" -F "variables[AGRUM_BUILD]=1" -F "variables[DEPLOY_TO_PYPI]=TRUE" -F "variables[DEPLOY_DOC]=TRUE"  https://gitlab.com/api/v4/projects/4935470/trigger/pipeline
  retry: 1
